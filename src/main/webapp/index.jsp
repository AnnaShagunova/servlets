<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <title>JSP - Hello World</title>
    </head>
    <body>
        <h1><%= "Hello World!" %>
        </h1>
        <br/>
        <a href="hello-servlet">Hello Servlet</a>
        <br/>
        <br/>
        <a href="first-servlet">It's working?</a>
        <br/>
        <br/>
        <a href="testJSP.jsp">First JSP</a>
        <br/>
        <br/>
        <a href="servletSession">Counter and Card test (session)</a>
        <br/>
        <br/>
        <a href="get-cookies">Get some cookies?</a>
        <br/>
        <a href="set-cookies">Set some cookies?</a>
        <br/>
        <a href="delete-cookies">No cookies?</a>
        <br/>
        <br/>
        <a href="products">Products</a>

    </body>
</html>