<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>

<html>
    <head>
        <title>First JSP</title>
    </head>
    <body>
        <h1>Testing JSP</h1>
        <p>
        <%--клиент отправил параметр name--%>
        <%--получение параметра--%>
            <% String name = request.getParameter("name"); %>
            <%= "Hello, " + name %>
            <br/>
            <br/>
            <%@ page import="com.example.servlets.ForTestJSP" %>
            <% ForTestJSP testJSP = new ForTestJSP(); %>
            <%= testJSP.getInfo() %>
        </p>
    </body>
</html>
