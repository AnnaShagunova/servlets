<%@ page import="com.example.servlets.session.Card" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<html>
    <head>
        <title>Card</title>
    </head>
    <body>
        <% Card card = (Card) session.getAttribute("card"); %>
        <p>Product name: <%= card.getName()%> </p>
        <p>Quantity: <%= card.getQuantity()%> </p>
    </body>
</html>
