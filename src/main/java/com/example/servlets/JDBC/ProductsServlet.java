package com.example.servlets.JDBC;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet(name = "ProductsServlet", value = "/products")
public class ProductsServlet extends HttpServlet {

    private static final String URL = "jdbc:mysql://localhost:3306/java_ee_db";
    private static final String USERNAME = "root";
    private static final String PASSWORD = "root";

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setCharacterEncoding("UTF-8");
        PrintWriter pw = response.getWriter();
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);

            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM products");

            pw.println("<html>");
            pw.println("<p>Name products -  price</p>");
            while (resultSet.next()){
                String name = resultSet.getString("name");
                String price = resultSet.getString("price");
                pw.println("<p>" + name + " - " + price + "</p>");
            }
            pw.println("</html>");

            if(!connection.isClosed()){
                System.out.println("Соединение с БД установлено!");
            }
            connection.close();
            if(connection.isClosed()){
                System.out.println("Соединение с БД закрыто!");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
