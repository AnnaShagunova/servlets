package com.example.servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "firstServlet", value = "/first-servlet")
public class FirstServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //клиент отправил параметр name
        //получение параметра
        String name = request.getParameter("name");
        String lastname = request.getParameter("lastname");

        //Пример вкрапления html кода в java файле
        PrintWriter pw = response.getWriter();
        pw.println("<html>");
        pw.println("<h1> Hello, " + name + " " + lastname + "</h1>");
        pw.println("<h2> It's working! </h2>");
        pw.println("</html>");

        //Redirect
//        response.sendRedirect("https://gitlab.com/AnnaShagunova/servlets");
//        response.sendRedirect("/Servlets_war/first.jsp");

        //Forward
        RequestDispatcher dispatcher = request.getRequestDispatcher("/first.jsp");
        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            throw new RuntimeException(e);
        }
    }
}
