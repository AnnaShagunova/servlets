package com.example.servlets.session;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ServletSession", value = "/servletSession")
public class ServletSession extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        //Пример 1: счетчик посещений страницы определенным пользователем
        Integer count = (Integer) session.getAttribute("count");
        if(count == null){
            session.setAttribute("count", 1);
            count = 1;
        }else {
            session.setAttribute("count", ++count);
        }
        PrintWriter pw = response.getWriter();
        pw.println("<html>");
        pw.println("<h1> Your count is: " + count + "</h1>");
        pw.println("</html>");


        //Пример 2: объекты в корзине пользователя
        Card card = (Card) session.getAttribute("card");

        String name = request.getParameter("name");
        int quantity = Integer.parseInt(request.getParameter("quantity"));

        if(card == null){
            card = new Card();
            card.setName(name);
            card.setQuantity(quantity);
        }
        session.setAttribute("card", card);

        getServletContext().getRequestDispatcher("/showCard.jsp").forward(request, response);

        //Пример 3: Проверяем был ли авторизован пользователь (не авторизован -> регистрация; аторизован -> домашняя стр)
        String user = (String) session.getAttribute("current_user");
        if(user == null){
            /* response для ананимного пользователя
             аторизация/регистрация
             session.setAttribute("current_user", ID);
            */
        }else {
            // response для авторизованного пользователя
        }
    }
}
